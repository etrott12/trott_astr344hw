import numpy as np
import collections

runs = 20000.0
for i in range(25):
    repeats = 0
    #generates an array of size (runs,i) populated by random integers from 1 to 365
    bday = np.random.random_integers(1,366,(int(runs),i))
    for k in range(int(runs)):
        bday_row = bday[k,:]
        #outputs the number of times that each integer occurs in each row
        dupes = collections.Counter(bday_row)
        for a in dupes:
            if dupes[a] > 1:
                repeats += 1
                break
    print i,'people have a',repeats/runs,'repeat rate'
    if repeats/runs > 0.5:
        print 'The smallest number of people in a room for the probability to be greater than 0.5 that two people in the group have the same birthday is',i
        break
