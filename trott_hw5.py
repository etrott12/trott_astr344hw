import numpy as np
import matplotlib.pyplot as plt

#Define Functions
def f(x):
    return np.sin(x)

def g(x):
    return (x**3)-x-2

def y(x):
    return -6+x+(x**2)

#Set Tolerance
tol = 10**-3

#Find and plot root for f(x)
x1 = 0.1
x2 = 1.1
while abs(x1 - x2) > tol:
    if f((x1+x2)/2)/f(x2) < 0:
        x1 = (x1+x2)/2
        print  'f(x) x1 =', x1
    elif f(x1)/f((x1+x2)/2) < 0:
        x2 = (x1+x2)/2
        print 'f(x) x2 =',x2
    else:
        x2 *= 2
        x1 *= 2
f_range = np.arange(2.0,4.0,0.01)
f_list = []
for i in range(len(f_range)):
    f_list.append(0)
plt.plot(f_range,f(f_range),'r',f_range,f_list,'k',x1,f(x1),'bo')
plt.title('F(x) Root Finding')
plt.xlabel('x')
plt.ylabel('sin(x)')
plt.savefig('/students/etrott/Trott_ASTR344HW/trott_hw5_fplot.png')
plt.close()

#Find and plot root for g(x)
x1 = 0.1
x2 = 1.1
while abs(x1 - x2) > tol:
    if g((x1+x2)/2)/g(x2) < 0:
        x1 = (x1+x2)/2
        print  'g(x) x1 =', x1
    elif g(x1)/g((x1+x2)/2) < 0:
        x2 = (x1+x2)/2
        print 'g(x) x2 =',x2
    else:
        x2 *= 2
        x1 *= 2
g_range = np.arange(1.0,2.0,0.01)
g_list = []
for i in range(len(g_range)):
    g_list.append(0)
plt.plot(g_range,g(g_range),'r',g_range,g_list,'k',x1,g(x1),'bo')
plt.title('G(x) Root Finding')
plt.xlabel('x')
plt.ylabel('x^3-x-2')
plt.savefig('/students/etrott/Trott_ASTR344HW/trott_hw5_gplot.png')
plt.close()

#Find and plot root for y(x)
x1 = 0.1
x2 = 1.1
while abs(x1 - x2) > tol:
    if y((x1+x2)/2)/y(x2) < 0:
        x1 = (x1+x2)/2
        print  'y(x) x1 =', x1
    elif y(x1)/y((x1+x2)/2) < 0:
        x2 = (x1+x2)/2
        print 'y(x) x2 =',x2
    else:
        x2 *= 2
        x1 *= 2
y_range = np.arange(0.0,5.0,0.01)
y_list = []
for i in range(len(y_range)):
    y_list.append(0)
plt.plot(y_range,y(y_range),'r',y_range,y_list,'k',x1,y(x1),'bo')
plt.title('Y(x) Root Finding')
plt.xlabel('x')
plt.ylabel('x^2+x-6')
plt.savefig('/students/etrott/Trott_ASTR344HW/trott_hw5_yplot.png')
plt.close()
