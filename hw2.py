import numpy as np

sum1 = 0
real_sum1 = 0
for i in range(0,6):
    x_real = np.float64((1./3.)**i)
    x = np.float32((1./3.)**i)
    sum1 += x
    real_sum1 += x_real
n5_absolute_error = real_sum1 - sum1
n5_relative_error = (real_sum1 - sum1)/real_sum1
print "n = 5, (1/3)^n absolute error:", n5_absolute_error
print "n = 5, (1/3)^n relative error:", n5_relative_error

sum2 = 0
real_sum2 = 0
for i in range(0,21):
    y = np.float32((1./3.)**i)
    real_y = np.float64((1./3.)**i)
    sum2 += y
    real_sum2 += real_y
n20_absolute_error = real_sum2 - sum2
n20_relative_error = (real_sum2 - sum2)/real_sum2
print "n = 20, (1/3)^n absolute error:", n20_absolute_error
print "n = 20, (1/3)^n relative error:", n20_relative_error

sum3 = 0
real_sum3 = 0
for i in range(0,21):
    z = np.float32(4.**i)
    real_z = np.float64(4.**i)
    sum3 += z
    real_sum3 += real_z
n20_absolute_error = real_sum3 - sum3
n20_relative_error = (real_sum3 - sum3)/real_sum3
print "n = 20, 4^n absolute error:", n20_absolute_error
print "n = 20, 4^n relative error:", n20_relative_error

print "This calculation is stable because 4 is not affected by truncation whereas 1/3 is due to it being a non-terminating decimal. Relative error should be used to measure accuracy and stability because they both need context. For example, an absolute error of 5 would be terrible if the correct answer is 10 but great if the correct answer is 10 million."

