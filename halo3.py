import numpy as np
import random
import math
from scipy import spatial
import matplotlib.pyplot as plt

t = 3.983e6
dt = 1820034.0
z = 249.0
a = 1.0/(1.0+z)
m = 2.0e42

#calculate component velocity changes due to gravitational forces
def x_move(x,y,z,a):
    delta_vx = -((6.674e-11)*m*dt*x)/((x**2+y**2+z**2)**(3.0/2.0)+195312.5*a)
    return delta_vx
def y_move(x,y,z,a):
    delta_vy = -((6.674e-11)*m*dt*y)/((x**2+y**2+z**2)**(3.0/2.0)+195312.5*a)
    return delta_vy
def z_move(x,y,z,a):
    delta_vz = -((6.674e-11)*m*dt*z)/((x**2+y**2+z**2)**(3.0/2.0)+195312.5*a)
    return delta_vz

#calculates scale factor
def scale(t):
    a_new = 0.753947*(math.sinh((2.50998e-18)*(t+dt))**(2.0/3.0))*114285.7
    return a_new

counter = 0

#position lists
x_pos = []
y_pos = []
z_pos = []

#velocity lists
x_vel = []
y_vel = []
z_vel = []

for i in range(128):
    #initialize random positions
    x_pos.append(random.randrange(0,1000,1))
    y_pos.append(random.randrange(0,1000,1))
    z_pos.append(random.randrange(0,1000,1))
    #initialize zero velocities
    x_vel.append(0)
    y_vel.append(0)
    z_vel.append(0)

while z > 6:
    #distance lists
    x_dist = []
    y_dist = []
    z_dist = []
    #calculates the distance between each particle
    for i in range(128):
        for j in range(128):
            x_dist.append(x_pos[i]-x_pos[j])
            y_dist.append(y_pos[i]-y_pos[j])
            z_dist.append(z_pos[i]-z_pos[j])

    #velocity change lists
    dvx_list = []
    dvy_list = []
    dvz_list = []
    #calculates the velocity change from each particle interaction
    for i in range(len(x_dist)):
        dvx = x_move(x_dist[i],y_dist[i],z_dist[i],a)
        dvy = y_move(x_dist[i],y_dist[i],z_dist[i],a)
        dvz = z_move(x_dist[i],y_dist[i],z_dist[i],a)
        dvx_list.append(dvx)
        dvy_list.append(dvy)
        dvz_list.append(dvz)

    #dummy variables
    dvx_sum = 0
    dvy_sum = 0
    dvz_sum = 0
    #particle velocity lists (each entry is the net velocity change of a particle)
    dvx_net = []
    dvy_net = []
    dvz_net = []
    #sums over each 128 elements to find the net velocity change of each particle
    for i in range(0,len(dvx_list),128):
        for j in range(i,128+i):
            dvx_sum += dvx_list[j]
            dvy_sum += dvy_list[j]
            dvz_sum += dvz_list[j]
        dvx_net.append(dvx_sum)
        dvy_net.append(dvy_sum)
        dvz_net.append(dvz_sum)

    #changes each particle's velocity
    for i in range(len(x_vel)):
        x_vel[i] += dvx_net[i]
        y_vel[i] += dvy_net[i]
        z_vel[i] += dvz_net[i]

    #changes each particle's position
    for i in range(len(x_pos)):
        x_pos[i] += x_vel[i]*dt/m
        y_pos[i] += y_vel[i]*dt/m
        z_pos[i] += z_vel[i]*dt/m
        #randomizes component position if the particle has wandered out of the box
        if abs(x_pos[i]) > 1000*250*a:
            x_pos[i] = random.randrange(0,1000,1)
        if abs(y_pos[i]) > 1000*250*a:
            y_pos[i] = random.randrange(0,1000,1)
        if abs(z_pos[i]) > 1000*250*a:
            z_pos[i] = random.randrange(0,1000,1)

    #modifies all positions by the scale factor
    for i in range(len(x_pos)):
        x_pos[i] = (x_pos[i]/a)*scale(t)
        y_pos[i] = (y_pos[i]/a)*scale(t)
        z_pos[i] = (z_pos[i]/a)*scale(t)

########################################################
###################### PLOTTING ########################
########################################################

    #plot timestep 0
    if counter == 0:
        #xy plot
        for i in range(len(x_pos)):
            plt.plot(x_pos[i],y_pos[i],'b.')
            #plt.axis([0,35000,0,35000])
            plt.xlabel('x position (kpc)')
            plt.ylabel('y position (kpc)')
            plt.title('Cosmological Simulation z = 249')
        plt.savefig('halo_xy0.png')
        plt.close('all')

        #xz plot
        for i in range(len(x_pos)):
            plt.plot(x_pos[i],z_pos[i],'r.')
            #plt.axis([0,35000,0,35000])
            plt.xlabel('x position (kpc)')
            plt.ylabel('z position (kpc)')
            plt.title('Cosmological Simulation z = 249')
        plt.savefig('halo_xz0.png')
        plt.close('all')

    #plot timestep 99
    if counter == 99:
        #xy plot
        for i in range(len(x_pos)):
            plt.plot(x_pos[i],y_pos[i],'b.')
            #plt.axis([0,35000,0,35000])
            plt.xlabel('x position (kpc)')
            plt.ylabel('y position (kpc)')
            plt.title('Cosmological Simulation z = 18.4')
        plt.savefig('halo_xy99.png')
        plt.close('all')

        #xz plot
        for i in range(len(x_pos)):
            plt.plot(x_pos[i],z_pos[i],'r.')
            #plt.axis([0,35000,0,35000])
            plt.xlabel('x position (kpc)')
            plt.ylabel('z position (kpc)')
            plt.title('Cosmological Simulation z = 18.4')
        plt.savefig('halo_xz99.png')
        plt.close('all')

    #plot timestep 199
    if counter == 199:
        #xy plot
        for i in range(len(x_pos)):
            plt.plot(x_pos[i],y_pos[i],'b.')
            #plt.axis([0,35000,0,35000])
            plt.xlabel('x position (kpc)')
            plt.ylabel('y position (kpc)')
            plt.title('Cosmological Simulation z = 11.3')
        plt.savefig('halo_xy199.png')
        plt.close('all')

        #xz plot
        for i in range(len(x_pos)):
            plt.plot(x_pos[i],z_pos[i],'r.')
            #plt.axis([0,35000,0,35000])
            plt.xlabel('x position (kpc)')
            plt.ylabel('z position (kpc)')
            plt.title('Cosmological Simulation z = 11.3')
        plt.savefig('halo_xz199.png')
        plt.close('all')

    #plot timestep 299
    if counter == 299:
        #xy plot
        for i in range(len(x_pos)):
            plt.plot(x_pos[i],y_pos[i],'b.')
            #plt.axis([0,35000,0,35000])
            plt.xlabel('x position (kpc)')
            plt.ylabel('y position (kpc)')
            plt.title('Cosmological Simulation z = 8.4')
        plt.savefig('halo_xy299.png')
        plt.close('all')

        #xz plot
        for i in range(len(x_pos)):
            plt.plot(x_pos[i],z_pos[i],'r.')
            #plt.axis([0,35000,0,35000])
            plt.xlabel('x position (kpc)')
            plt.ylabel('z position (kpc)')
            plt.title('Cosmological Simulation z = 8.4')
        plt.savefig('halo_xz299.png')
        plt.close('all')

    #plot timestep 399
    if counter == 399:
        #xy plot
        for i in range(len(x_pos)):
            plt.plot(x_pos[i],y_pos[i],'b.')
            #plt.axis([0,35000,0,35000])
            plt.xlabel('x position (kpc)')
            plt.ylabel('y position (kpc)')
            plt.title('Cosmological Simulation z = 6.7')
        plt.savefig('halo_xy399.png')
        plt.close('all')

        #xz plot
        for i in range(len(x_pos)):
            plt.plot(x_pos[i],z_pos[i],'r.')
            #plt.axis([0,35000,0,35000])
            plt.xlabel('x position (kpc)')
            plt.ylabel('z position (kpc)')
            plt.title('Cosmological Simulation z = 6.7')
        plt.savefig('halo_xz399.png')
        plt.close('all')

    #plot timestep 465
    if counter == 465:
        #xy plot
        for i in range(len(x_pos)):
            plt.plot(x_pos[i],y_pos[i],'b.')
            #plt.axis([0,35000,0,35000])
            plt.xlabel('x position (kpc)')
            plt.ylabel('y position (kpc)')
            plt.title('Cosmological Simulation z = 6')
        plt.savefig('halo_xy499.png')
        plt.close('all')

        #xz plot
        for i in range(len(x_pos)):
            plt.plot(x_pos[i],z_pos[i],'r.')
            #plt.axis([0,35000,0,35000])
            plt.xlabel('x position (kpc)')
            plt.ylabel('z position (kpc)')
            plt.title('Cosmological Simulation z = 6')
        plt.savefig('halo_xz499.png')
        plt.close('all')

    #update scale factor for the next cycle
    a = scale(t)

    #updates time for next cycle (must do after updating scale factor)
    t += dt

    #update z for the while loop to check against
    z = (1/a)-1

    counter += 1
