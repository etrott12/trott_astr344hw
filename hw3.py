import numpy as np
import matplotlib.pyplot as plt
from astropy.io import ascii
from astropy.table import Table, Column
from math import pi

model_smg = ascii.read('/students/etrott/Trott_ASTR344HW/astr344_homework_materials/model_smg.dat')

luminosity_data = []
number_data = []
for i in range(len(model_smg)):
    luminosity_data.append(model_smg[i][0])
    number_data.append(model_smg[i][1])

ncounts_850 = ascii.read('/students/etrott/Trott_ASTR344HW/astr344_homework_materials/ncounts_850.dat')

log_luminosity_data = []
log_diff_number_data = []
for i in range(len(ncounts_850)):
    log_luminosity_data.append(ncounts_850[i][0])
    log_diff_number_data.append(ncounts_850[i][1])

dN_dL_list = []
luminosity_data_list = []
for i in range(1,len(luminosity_data)-1):
    h1 = luminosity_data[i]-luminosity_data[i-1]
    h2 = luminosity_data[i+1]-luminosity_data[i]
    dN_dL = (h1*number_data[i+1])/(h2*(h1+h2))-((h1-h2)*number_data[i])/(h1*h2)-(h2*number_data[i-1])/(h1*(h1+h2))
    dN_dL_list.append(np.log10(-dN_dL))
    luminosity_data_list.append(np.log10(luminosity_data[i]))

plt.plot(log_luminosity_data, log_diff_number_data, 'bo', label = 'Survey Data')
plt.plot(luminosity_data_list, dN_dL_list, 'ro', label = 'Model Data')
plt.xlabel('log(L)')
plt.ylabel('log(dN/dL)')
plt.legend()
plt.title('Change in Galaxies Detected as a Function of Luminosity')
plt.savefig('/students/etrott/Trott_ASTR344HW/hw3_plot.png')
