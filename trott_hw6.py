from datetime import datetime
from datetime import timedelta
import numpy as np
import matplotlib.pyplot as plt

#Define Functions
def f(x):
    return np.sin(x)

def g(x):
    return (x**3)-x-2

def y(x):
    return -6+x+(x**2)
def der(a):
    dx = 0.1
    return (a(x+dx)-a(x))/dx
#Set Tolerance
tol = 10**-3

#Find and plot root for f(x)
x1 = 0.1
x2 = 1.1
f_bisect_time1 = datetime.now()
while abs(x1 - x2) > tol:
    if f((x1+x2)/2)/f(x2) < 0:
        x1 = (x1+x2)/2
        #print  'f(x) x1 =', x1
    elif f(x1)/f((x1+x2)/2) < 0:
        x2 = (x1+x2)/2
        #print 'f(x) x2 =',x2
    else:
        x2 *= 2
        x1 *= 2
f_bisect_time2 = datetime.now()
print 'f(x) bisection time =', f_bisect_time2 - f_bisect_time1

f_nr_time1 = datetime.now()
x = 2.0
while abs(f(x)) > tol:
    x = x - f(x)/der(f)
f_nr_time2 = datetime.now()
print 'f(x) NR time =', f_nr_time2 - f_nr_time1
print 'NR is faster than bisection'

#Find and plot root for g(x)
x1 = 0.1
x2 = 1.1
g_bisect_time1 = datetime.now()
while abs(x1 - x2) > tol:
    if g((x1+x2)/2)/g(x2) < 0:
        x1 = (x1+x2)/2
        #print  'g(x) x1 =', x1
    elif g(x1)/g((x1+x2)/2) < 0:
        x2 = (x1+x2)/2
        #print 'g(x) x2 =',x2
    else:
        x2 *= 2
        x1 *= 2
g_bisect_time2 = datetime.now()
print 'g(x) bisection time =', g_bisect_time2 - g_bisect_time1

g_nr_time1 = datetime.now()
x = 0.1
while abs(g(x)) > tol:
    x = x - g(x)/der(g)
g_nr_time2 = datetime.now()
print 'g(x) NR time =', g_nr_time2 - g_nr_time1
print 'bisection is faster than NR'

#Find and plot root for y(x)
x1 = 0.1
x2 = 1.1
y_bisect_time1 = datetime.now()
while abs(x1 - x2) > tol:
    if y((x1+x2)/2)/y(x2) < 0:
        x1 = (x1+x2)/2
        #print  'y(x) x1 =', x1
    elif y(x1)/y((x1+x2)/2) < 0:
        x2 = (x1+x2)/2
        #print 'y(x) x2 =',x2
    else:
        x2 *= 2
        x1 *= 2
y_bisect_time2 = datetime.now()
print 'y(x) bisection time =', y_bisect_time2 - y_bisect_time1

y_nr_time1 = datetime.now()
x = 0.1
while abs(y(x)) > tol:
    x = x - y(x)/der(y)
y_nr_time2 = datetime.now()
print 'y(x) NR time =', y_nr_time2 - y_nr_time1
print 'NR is faster than bisection'

######################Part 2#######################


h = 6.6260755*10**-27
c = 2.99792458*10**10
v = c/0.087
k = 1.380658*10**-16

def B(T):
    return ((2.0*h*v**3.0)/(c**2.0))/(np.exp((h*v)/(k*T))-1.0)-1.25e-12

def derivative(B):
    dT = 0.1
    return (B(T+dT) - B(T))/dT

T = 1.0
while abs(B(T)) > 1e-14:
    T = T - B(T)/derivative(B)
print 'T =', T
