import numpy as np
import ipdb
from scipy import spatial
import matplotlib.pyplot as plt
from datetime import datetime
from datetime import timedelta

class Person(object):
    def __init__(self, status = 'healthy', x_position ='', y_position = ''):
        self.status = status
        self.x_position = x_position
        self.y_position = y_position
    def place(self):
        self.x_position = np.random.random()
        self.y_position = np.random.random()
    def move(self):
        dx = 0.05*np.random.random()*np.cos(360*np.random.random())
        dy = 0.05*np.random.random()*np.sin(360*np.random.random())
        self.x_position += dx
        self.y_position += dy
        if self.x_position > 1 or self.x_position < 0:
            self.x_position = np.random.random()
        if self.y_position > 1 or self.x_position < 0:
            self.y_position = np.random.random()

Emery = Person()
Emery.place()
Matthew = Person()
Matthew.place()
Derrick = Person()
Derrick.place()
Sarah = Person()
Sarah.place()
E_x_move = []
E_y_move = []
M_x_move = []
M_y_move = []
D_x_move = []
D_y_move = []
S_x_move = []
S_y_move = []
for i in range(100):
    Emery.move()
    Matthew.move()
    Derrick.move()
    Sarah.move()
    E_x_move.append(Emery.x_position)
    E_y_move.append(Emery.y_position)
    M_x_move.append(Matthew.x_position)
    M_y_move.append(Matthew.y_position)
    D_x_move.append(Derrick.x_position)
    D_y_move.append(Derrick.y_position)
    S_x_move.append(Sarah.x_position)
    S_y_move.append(Sarah.y_position)
plt.plot(E_x_move,E_y_move,'b')
plt.plot(M_x_move,M_y_move,'r')
plt.plot(D_x_move,D_y_move,'g')
plt.plot(S_x_move,S_y_move,'m')
plt.xlabel('X Position')
plt.ylabel('Y Position')
plt.xlim(0,1)
plt.ylim(0,1)
plt.title('Person Movement')
plt.savefig('trott_project1_unbuffered_movement.png')
