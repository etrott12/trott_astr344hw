import numpy as np
import matplotlib.pyplot as plt

x_location = 100 * np.random.random((100,1))
y_location = 100 * np.random.random((100,1))
run_time = 10
for i in range(run_time):
    r_movement = np.random.random((100,1))
    theta_movement = 360 * np.random.random((100,1))
    x_movement = r_movement * np.sin(theta_movement)
    y_movement = r_movement * np.cos(theta_movement)
    x_location += x_movement
    y_location += y_movement
    for i in range(len(x_location)):
        if x_location[i] > 100:
            x_location[i] = 100
        if y_location[i] > 100:
            y_location[i] > 100

def rand_exp(n):
    quantity = 1-np.random.random((n,1))
    x_plot = 1-quantity
    value = -np.log(quantity)
    #plt.plot(1-quantity,value)
    #plt.show()
    return value
#trial = rand_exp(100)

