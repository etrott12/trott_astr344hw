import numpy as np
import matplotlib.pyplot as plt

data = []
trapz_data = []
for i in range(0,500):
    z = i/100.0
    DA = 0
    DA_trapz = 0
    x_data = np.linspace(0,z,z*100)
    for i in range(len(x_data)-1):
        I = ((x_data[i+1]-x_data[i]))*((3000/np.sqrt(0.3*(1+x_data[i+1])**3+0.7))+(3000/np.sqrt(0.3*(1+x_data[i])**3+0.7)))/2
        trapz = np.trapz(y = [3000/np.sqrt(0.3*(1+x_data[i])**3+0.7),(3000/np.sqrt(0.3*(1+x_data[i+1])**3+0.7))], dx = 0.01)
        DA_trapz += (trapz/(1+z))
        DA += (I/(1+z))
    data.append(DA)
    trapz_data.append(DA_trapz)

for i in range(500):
    plt.plot(i/100.0, data[i], 'b|')
    plt.plot(i/100.0, trapz_data[i], 'r_')
    plt.title('Angular Diameter Distance as a Function of Redshift')
    plt.xlabel('Redshift')
    plt.ylabel('Angular Diameter Distance')
plt.savefig('/students/etrott/Trott_ASTR344HW/trott_hw4_plot.png')

print 'For some reason the code would not finish running when I tried to add a legend to the plot. The blue dashes are my numerical trapezoidal integration and the red dashes are from trapz.'
