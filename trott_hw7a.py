import numpy as np
print 'takes about 20 seconds'
for a in range(1,6):
    N = float(a*2000000)
    hit = 0
    for i in range(int(N)):
        x = 2*np.random.random()
        y = 2*np.random.random()
        if (x-1)**2 + (y-1)**2 <= 1:
            hit += 1
            area = 4*hit/N
    print N,'iterations gives pi =',area,'with an error of',abs(3.141-area)
print 'Seems to converge on most runs to 3.141 for N = 10 Million'
