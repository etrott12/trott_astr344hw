import numpy as np
import ipdb
from scipy import spatial
import matplotlib.pyplot as plt
from datetime import datetime
from datetime import timedelta

class Person(object):
    def __init__(self, status = 'healthy', x_position ='', y_position = ''):
        self.status = status
        self.x_position = x_position
        self.y_position = y_position
    def place(self):
        self.x_position = np.random.random()
        self.y_position = np.random.random()
    def move(self):
        dx = 0.05*np.random.random()*np.cos(360*np.random.random())
        dy = 0.05*np.random.random()*np.sin(360*np.random.random())
        self.x_position += dx
        self.y_position += dy
        if self.x_position > 1 or self.x_position < 0:
            self.x_position = np.random.random()
        if self.y_position > 1 or self.x_position < 0:
            self.y_position = np.random.random()
"""
Emery = Person()
Emery.place()
print 'initial x position',Emery.x_position
print 'initial y position',Emery.y_position
for i in range(10):
    Emery.move()
    print 'x position',Emery.x_position
    print 'y position',Emery.y_position
"""
beginning = datetime.now()

num_people = 100
num_runs = 100
time_list = []
for num_vac in range(num_people-1):
    for a in range(num_runs):
#new person list for each run
        person_list = []
        x_list = []
        y_list = []
#fills a list of people of size num_people, and initiates positions
        for e in range(num_people):
            human = Person()
            human.place()
            person_list.append(human)
            x_list.append(human.x_position)
            y_list.append(human.y_position)
        x_pos = np.asarray(x_list)
        y_pos = np.asarray(y_list)
#infect the last person
        person_list[num_people-1].status = 'infected'
        infected_list = []
        vaccinated_list = []
        infected_list.append(person_list[num_people-1])
#vaccinates num_vac number of people from the beginning of the list
        for i in range(num_vac):
            person_list[i].status = 'vaccinated'
            vaccinated_list.append(person_list[i])
        time = 0
        print 'target =',num_people - len(vaccinated_list),'iteration',a
        while len(infected_list) < num_people - len(vaccinated_list):
            #print 'time =',time
            #print len(infected_list),'infected'
            #print 'target =',num_people - len(vaccinated_list)
            time += 1
#zips the two arrays into a single array of tuples
            data = zip(x_pos.ravel(),y_pos.ravel())
            #print 'data =',data,'at time =',time
#creates a tree out of data
            tree = spatial.KDTree(data)
            for u in range(len(data)):
                #print 'entering data loop'
#finds the indeces in data of all points within distance 0.05 of data[u]
                kd_tree = tree.query_ball_point(data[u],r = 0.05)
                gate = 0
#if kd tree has more than one index (since it returns the index of the comparison point too) then analyze the status of the people with those indeces
                if len(kd_tree) > 1:
                    #print 'entering kd tree loop'
#if one of those people is infected then analyze the people who are close to him
                    for y in range(len(kd_tree)):
                        if person_list[kd_tree[y]].status == 'infected':
                            gate = 1
                        if gate == 1:
#if the status of someone near an infected person is 'healthy' then there is a chance for infection
                            for a in range(len(kd_tree)):
                                #print len(infected_list),'people infected'
                                #print person_list[kd_tree[a]].status
                                if person_list[kd_tree[a]].status == 'healthy':
                                    #print 'initiated infection!!!'
                                    if np.random.random() < 0.75:
                                        person_list[kd_tree[a]].status = 'infected'
                                        #print 'INFECTION SUCCESSFUL'
                                        infected_list.append(person_list[kd_tree[a]])
                                        #print 'altered status to',person_list[kd_tree[a]].status
            x_list = []
            y_list = []
            #print person_list[0].x_position
            for person in person_list:
                person.move()
                x_list.append(person.x_position)
                y_list.append(person.y_position)
            x_pos = np.asarray(x_list)
            y_pos = np.asarray(y_list)
            
        time_list.append(time)
lower = 0
my_list = []
while lower + num_runs <= num_runs * (num_people-1):
    row = []
    for b in range(lower,lower + num_runs):
        row.append(time_list[b])
    array = np.asarray(row)
    avg = np.average(array)
    my_list.append(avg)
    lower += num_runs
print my_list
x_data = []
for f in range(num_people - 1):
    x_data.append(f)

plt.plot(x_data,my_list,'bo')
plt.xlabel('Number of People Vaccinated')
plt.ylabel('Code Time to Total Possible Infections')
plt.title('Infection Time vs. Number of People Vaccinated (Without Buffering)')
plt.savefig('trott_project1_plot1.png')

end = datetime.now()
run_time = end - beginning
print run_time
